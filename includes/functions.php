<?php

/**
 * 
 * @param type $string_paragraph to be filtered to find the keywords.
 * @return type Array of suggested keywords
 */
function unique_word_count($string_paragraph) {
    $array_paragraph = explode(' ', strtolower($string_paragraph));
    $word_array = array_count_values($array_paragraph);    
    return remove_keywords($word_array);
}

/*
 * This function will remove the omitted keywords.
 * @Param type $array = String where the words to be removed.
 * 
 * Return: Array of string with suggested keywords
 */
function remove_keywords($array){
    $remove =  file_get_contents("includes/remove.txt"); //we can add more removable dictionary.
    $remove_array = explode(",", $remove);
    $return_array=array();
    
    foreach ($array as $key=>$value) { //loop through original string
        
        if(strlen($key)>2 && in_array($key, $remove_array)==0){
            //only add to return array if the word not to be removed.
            $return_array[]= htmlspecialchars(strip_tags($key));
        }
                
    }
    return $return_array;
}
