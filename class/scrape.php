<?php

/**
 * Description of scrape
 *
 * @author Kishor
 */
require_once 'includes/functions.php'; //import the functions
require 'includes/simple_html_dom.php'; //reference to the simple_html_dom functions

class scrape {

    function scrape() {
        
    }

    function scrape_url($url) {
        $html = file_get_html($url); //download the page contents and store it in html variable

        $digitalia = false; //used to identify if the post category is digitalia, default set to  'false'
        
        //now loop throught the download file and find the class entry-wrap as the entries are
        //inside this div class.
        $totalsize = 0; //to calculate the full size of the results 
        foreach ($html->find(".entry-wrap") as $element) {
            //now check if there are any div.entry-footer available, if so, is there any 'Digitalia' in that element

            foreach ($element->find(".entry-footer") as $footer) {
                if (strpos($footer, "Digitalia")) {
                    $digitalia = true; //this entry is digitalia category, so we need to capture it from div.entry-summary            
                }
            }

            if ($digitalia) {
                //now read the entry-summary div
                foreach ($element->find(".entry-summary") as $entry_summary) {
                    //echo "<br>summary= ".$entry_summary."<br><br>";
                    foreach ($entry_summary->find("li") as $li) {

                        foreach ($li->find("a") as $link) {
                            //  echo "<br>Link=".$link->href."<br>";
                        }
                        //read the link address
                        $link_url = $link->href;

                        $link_title = $link->innertext; //read the link displayed text

                        $meta = str_replace($link, "", $li->innertext); //contents summary given for that link, removing the <a> contents as its not required here.
                        $meta = str_replace("<br />", "", $meta); //remove the <br /> tag.

                        $keywords = implode(",", unique_word_count($link_title . ' ' . $meta));

                        $byte = strlen(file_get_contents($link_url)); //read the url size
                        $filesize = number_format($byte / 1024, 2); //identify the filesize and convert to 
                        //create an array to add to elastic search
                        $params = array(
                            'url' => $link_url,
                            'link' => $link_title,
                            'meta description' => $meta,
                            'keywords' => $keywords,
                            'filesize' => $filesize . "kb"
                        );
                        $totalsize+=$filesize;
                        $results_out[] = $params;
                    }
                }
            }
        }
        //now add the total size of all the links
        $output = Array(
            "results" => $results_out,
            "total" => $totalsize . "kb"
        );
        
        return $output;
    }

}
