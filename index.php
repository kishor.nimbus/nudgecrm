<?php

$results="";
if(isset($_POST['scrape']) && intval($_POST['scrape'])==1){
    //import the scrape.php class
    require_once 'class/scrape.php';
    $scrape = new scrape(); //create a new instance 

    
    $url=  filter_input(INPUT_POST, 'url'); //url to be used to crawl
    $scraped_output = $scrape->scrape_url($url);

    $results = 1;
}
?>
<!doctype html>
<html>
    <head>
        <title>Scrape Web page</title>
        <style>
            #myform{
                width: 100%;
                text-align: center;
            }
            #resultArea{
                margin-top: 30px;
                background-color: silver;
                padding: 8px;
                border-radius: 8px;
                word-wrap: normal;
            }
            #resultArea h1{
                text-align: center;
                width: 90%;
            }
            
        </style>
    </head>
    <body>
        <div id="formArea">
        <form id="myform" name="myform" action="" method="post">
            <input type="hidden" name="scrape" value="1" />
            <input type="url" name="url" readonly="" id="url" style="width: 90%;padding: 20px;text-align: center;" value="https://www.black-ink.org/" />
            <br>
            <input type="submit" value="Scrape" />
        </form>
        </div>
        <div id="resultArea">
            <?php if($results==0){?>
                <h1>Please type the url to be scraped</h1>
            <?php }else{
                echo "<pre>". json_encode($scraped_output, JSON_PRETTY_PRINT )."</pre>";
            }?>
            
        </div>
    </body>
</html>
